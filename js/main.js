$(document).ready(function(){
    $('.slider-banner').owlCarousel( {
        items : 1,
        nav : true,
        navText : "",
        dots: false,
        loop : true,
        autoplay : true,
        autoplayHoverPause : true,
        fluidSpeed : 5000,
        autoplaySpeed : 1600,
        navSpeed : 1600,
        dotsSpeed : 1600,
        dragEndSpeed : 1600
    });

    $('.carousel-partners').owlCarousel( {
        items : 4,
        nav : false,
        navText : "",
        dots: false,
        loop : true,
        autoplay : true,
        autoplayHoverPause : true,
        fluidSpeed : 5000,
        autoplaySpeed : 1600,
        navSpeed : 1600,
        dotsSpeed : 1600,
        dragEndSpeed : 1600,
        responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.
            0:{
              items:1
            },
            480:{
                items:2
            },
            570:{
                items:3
            },
            768:{
                items:4
            }
        }
    });
});


$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-worker-preview'
});
$('.slider-worker-preview').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: false,
    nav: true,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 900,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 630,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                infinite: true,
            }
        }
    ]
});

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <ul id="horizontal-multilevel-menu" class="<?=$arParams['CSS_UL_CLASS']?>">

    <?
    $previousLevel = 0;
foreach ($arResult

    as $arItem): ?>
    <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
        <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
    <? endif ?>

    <? if ($arItem["IS_PARENT"]): ?>

    <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
    <li class="dropdown-menu-parent"><a href="<?= $arItem["LINK"] ?>"
                                        class="<? if ($arItem["SELECTED"]): ?>root-item-selected current-menu<? else: ?>root-item<? endif ?> dropdown-m"><?= $arItem["TEXT"] ?> <?= $arItem['PARAMS']['add_angle_down'] ?> </a>
    <ul class="dropdown-list">
    <? else: ?>
    <li class="dropdown-menu-parent"><a href="<?= $arItem["LINK"] ?>"
                                        class="parent <? if ($arItem["SELECTED"]): ?> current-menu<? endif ?> dropdown-m"><?= $arItem["TEXT"] ?> <?= $arItem['PARAMS']['add_angle_down'] ?> </a>
    <ul class="dropdown-list">
    <? endif ?>

    <? else: ?>

        <? if ($arItem["PERMISSION"] > "D"): ?>

            <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                <li><a href="<?= $arItem["LINK"] ?>"
                       class="<? if ($arItem["SELECTED"]): ?>root-item-selected current-menu<? else: ?>root-item<? endif ?>"><?= $arItem["TEXT"] ?></a>
                </li>
            <? else: ?>
                <li>
                    <a href="<?= $arItem["LINK"] ?>" <? if ($arItem["SELECTED"]): ?> class="current-menu"<? endif ?>><?= $arItem["TEXT"] ?></a>
                </li>
            <? endif ?>

        <? else: ?>

            <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                <li><a href="" class="<? if ($arItem["SELECTED"]): ?>current-menu<? else: ?>root-item<? endif ?>"
                       title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a></li>
            <? else: ?>
                <li><a href="" class="denied"
                       title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a></li>
            <? endif ?>

        <? endif ?>

    <? endif ?>

    <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

<? endforeach ?>

    <? if ($previousLevel > 1)://close last item tags?>
        <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
    <? endif ?>

    </ul>
    <div class="menu-clear-left"></div>
<? endif ?>